package testes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import conversor.Conversor;

public class ConversorTemperaturaTest {
	
	public Conversor meuConversor;
	
	@Before
	public void setUp() throws Exception{
		this.meuConversor = new Conversor();
	}
	
	@Test
	public void testFahrenheitCelsius(){
		assertEquals((-60/9), meuConversor.FahrenheitCelsius(20), 0.001);
	}
	
	@Test
	public void testFahrenheitCelsiusDouble(){
		assertEquals((-57.5/9), meuConversor.FahrenheitCelsius(20.5), 0.001);
	}
	
	@Test
	public void testCelsiusFahrenheit(){
		assertEquals(113, meuConversor.CelsiusFahrenheit(45), 0.001);
	}
	
	@Test
	public void testCelsiusFahrenheitDouble(){
		assertEquals(113.9, meuConversor.CelsiusFahrenheit(45.5), 0.01);
	}
	
	@Test
	public void testCelsiusKelvin(){
		assertEquals(280, meuConversor.CelsiusKelvin(7), 0.01);
	}
	
	@Test
	public void testCelsiusKelvinDouble(){
		assertEquals(286.5, meuConversor.CelsiusKelvin(13.5), 0.01);
	}
	
	@Test
	public void testFahrenheitKelvin(){
		assertEquals(283, meuConversor.FahrenheitKelvin(50), 0.01);
	}
	
	@Test
	public void testFahrenheitKelvinDouble(){
		assertEquals(278.5, meuConversor.FahrenheitKelvin(41.9), 0.01);
	}
	
	@Test
	public void testKelvinFahrenheit(){
		assertEquals(33.8, meuConversor.KelvinFahrenheit(274), 0.01);
	}
	
	@Test
	public void testKelvinFahrenheitDouble(){
		assertEquals(84.74, meuConversor.KelvinFahrenheit(302.3), 0.01);
	}
	
	@Test
	public void testKelvinCelsius(){
		assertEquals(2, meuConversor.KelvinCelsius(275), 0.01);
	}
	
	@Test
	public void testKelvinCelsiusDouble(){
		assertEquals(8.2, meuConversor.KelvinCelsius(281.2), 0.01);
	}
	
}
