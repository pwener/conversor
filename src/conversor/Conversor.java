package conversor;

public class Conversor {
	//Todos os métodos tem retorno do tipo double, pois por mais que a temperatura de entrada
	//seja inteira, a operação pode retornar um número quebrado.
	
	// Método de exemplo
	public double FahrenheitCelsius() {
		return (41 - 32) * 5 / 9;
	}

	public double FahrenheitCelsius(int temperatura) {
		return (temperatura - 32) * 5 / 9;
	}

	public double FahrenheitCelsius(double temperatura) {
		return (temperatura - 32) * 5 / 9;
	}

	// Método de exemplo
	public double CelsiusFahrenheit() {
		return (40 * 9 / 5) + 32;
	}

	public double CelsiusFahrenheit(int temperatura) {
		return (temperatura * 9 / 5) + 32;
	}

	public double CelsiusFahrenheit(double temperatura) {
		return (temperatura * 9 / 5) + 32;
	}

	// Método de exemplo
	public double CelsiusKelvin() {
		return 40 + 273;
	}

	public double CelsiusKelvin(int temperatura) {
		return temperatura + 273;
	}

	public double CelsiusKelvin(double temperatura) {
		return temperatura + 273;
	}

	// Método de exemplo
	public double KelvinCelsius() {
		return 40 - 273;
	}

	public double KelvinCelsius(int temperatura) {
		return temperatura - 273;
	}

	public double KelvinCelsius(double temperatura) {
		return temperatura - 273;
	}

	public double KelvinFahrenheit(double temperatura) {
		return CelsiusFahrenheit(KelvinCelsius(temperatura));
	}

	public double FahrenheitKelvin(double temperatura) {
		return CelsiusKelvin(FahrenheitCelsius(temperatura));
	}
	
	public double FahrenheitKelvin(int temperatura) {
		return CelsiusKelvin(FahrenheitCelsius(temperatura));
	}
}
