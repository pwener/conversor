package conversor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	public static void main(String[] args) throws NumberFormatException, IOException {
		InputStreamReader leitor = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(leitor);
		Conversor conversor = new Conversor();
		
		System.out.println("Digite a operação de conversão desejada:");
		System.out.println("1 - Fahrenheit para Celsius");
		System.out.println("2 - Celsius para Fahrenheit");
		System.out.println("3 - Celsius para Kelvin");
		System.out.println("4 - Kelvin para Celsius");
		System.out.println("5 - Kelvin para Fahrenheit");
		System.out.println("6 - Fahrenheit para Kelvin");
		
		int escolha = Integer.parseInt(br.readLine());
		System.out.println("Digite a temperatura que deseja converter");
		double temperatura = Double.parseDouble(br.readLine());
		
		switch(escolha){
			case 1:
				System.out.println(conversor.FahrenheitCelsius(temperatura));
				break;
			case 2:
				System.out.println(conversor.CelsiusFahrenheit(temperatura));
				break;
			case 3:
				System.out.println(conversor.CelsiusKelvin(temperatura));
				break;
			case 4:
				System.out.println(conversor.KelvinCelsius(temperatura));
				break;
			case 5:
				System.out.println(conversor.KelvinFahrenheit(temperatura));
				break;
			case 6:
				System.out.println(conversor.FahrenheitKelvin(temperatura));
				break;
		}
	}
}
